"use strict";

/* First define the Activity object "type", this should later
   on be move to separate file */
function Activity() {
  this.id = null;
  this.startTime = null;
  this.stopTime = null;
  this.labels = [];
  this.interruptions = [];
}

Activity.prototype.isActive = function() {
  return this.startTime != null && this.stopTime == null;
}
