"use strict";

function onError(error) {
  console.log(error);
}

function initialize() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities", "labels"]);
	getActivities.then(onGotData, onError);
}

function onGotData(storageResult) {
	console.log("Got activities and labels...", storageResult);

	// Get format from GET parameter
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let format = searchParams.get('format');

	if (format == "JSON") {
		let json = JSON.stringify(storageResult, null, 2);
		document.body.appendChild(document.createElement('pre')).innerHTML = json;
	} else {
		document.body.appendChild(document.createElement('pre')).innerHTML = 'Unknown format chosen. Choose "JSON"';
	}
}

function localStorageCallback() {
	console.log("Local storage has been updated.")
	window.location.reload(false);
}

browser.storage.onChanged.addListener(localStorageCallback);
initialize();
