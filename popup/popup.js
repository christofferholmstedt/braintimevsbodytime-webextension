// Open Log Activities page
//
function openLogActivities() {
	openExtensionPage("log_activities.html");
}

// Open Statistics page
//
function openStatistics() {
	openExtensionPage("statistics.html");
}

// Open Options page
//
function openOptions() {
	browser.runtime.openOptionsPage();
	window.close();
}

// Open About page
//
function openAbout() {
	openExtensionPage("about.html");
}

document.querySelector("#log_activity").addEventListener("click", openLogActivities);
document.querySelector("#statistics").addEventListener("click", openStatistics);
document.querySelector("#about").addEventListener("click", openAbout);
document.querySelector("#options").addEventListener("click", openOptions);
