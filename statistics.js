"use strict";

function onError(error) {
  console.log(error);
}

function initialize() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get("activities");
	getActivities.then(onGotActivities, onError);
}

function onGotActivities(storageResult) {
	console.log("Got activities...", storageResult);

	if (storageResult.activities === undefined)
	{
		populateStatisticsTable(null);
	} else {
		let tmpArray = [];
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		parseActivities(tmpArray);

		let statistics = getStatisticsFromActivities(tmpArray);
		populateStatisticsTable(statistics);
		let weeklyStatistics =  getStatisticsForWeeklyStats(tmpArray);
		populateWeeklyStatisticsTable(weeklyStatistics);
	}
}


function Statistics(timePeriod) {
  this.timePeriod = timePeriod;
  this.numberOfActivities = 0;
  this.uninterruptedHours = 0;
  this.totalTime = "00:00";
  this.eFactor= [];
}

function getStatisticsFromActivities(listOfActivities) {
	let result = [];

	let now = new Date();
	let twentyFourHoursAgo = new Date(now.getTime() - 24 * 3600 * 1000);
	let sevenDaysAgo = new Date(now.getTime() - 7 * 24 * 3600 * 1000);
	let thirtyDaysAgo = new Date(now.getTime() - 30 * 24 * 3600 * 1000);

	let statsToday = new Statistics("Today");
	let stats24Hours = new Statistics("Last 24 hours");
	let stats7Days = new Statistics("Last 7 days");
	let stats30Days = new Statistics("Last 30 days");
	let statsAll = new Statistics("All activities");
	let sumDeltasInMs = 0;
	let sumUninterruptedHours = 0;

	listOfActivities.forEach( function(activity) {

		if (!activity.isActive()) {
			sumDeltasInMs += getTotalTimeDeltaInMiliseconds(activity.startTime, activity.stopTime);
			sumUninterruptedHours += getUninterruptedHours(activity.startTime, activity.stopTime, activity.interruptions)
			let tmpTotalTime = getHoursAndMinutes(sumDeltasInMs)
			let tmpEFactor = getEFactor(sumUninterruptedHours, sumDeltasInMs)

			// Today / Current day
			if (activity.stopTime.getFullYear() == now.getFullYear() &&
				activity.stopTime.getMonth() == now.getMonth() &&
				activity.stopTime.getDate() == now.getDate()) {

				statsToday.numberOfActivities += 1;
				statsToday.uninterruptedHours = sumUninterruptedHours;
				statsToday.totalTime = getHoursAndMinutes(sumDeltasInMs);
				statsToday.eFactor = getEFactor(sumUninterruptedHours, sumDeltasInMs);
			}

			// Last 24 hours
			if (activity.stopTime.getTime() > twentyFourHoursAgo.getTime()) {
				stats24Hours.numberOfActivities += 1;
				stats24Hours.uninterruptedHours = sumUninterruptedHours;
				stats24Hours.totalTime = tmpTotalTime;
				stats24Hours.eFactor = tmpEFactor;
			}

			// Last 7 days
			if (activity.stopTime.getTime() > sevenDaysAgo.getTime()) {
				stats7Days.numberOfActivities += 1;
				stats7Days.uninterruptedHours = sumUninterruptedHours;
				stats7Days.totalTime = tmpTotalTime;
				stats7Days.eFactor = tmpEFactor;
			}

			// Last 30 days
			if (activity.stopTime.getTime() > thirtyDaysAgo.getTime()) {
				stats30Days.numberOfActivities += 1;
				stats30Days.uninterruptedHours = sumUninterruptedHours;
				stats30Days.totalTime = tmpTotalTime;
				stats30Days.eFactor = tmpEFactor;
			}

			// All activities
			statsAll.numberOfActivities += 1;
			statsAll.uninterruptedHours = sumUninterruptedHours;
			statsAll.totalTime = tmpTotalTime;
			statsAll.eFactor = tmpEFactor;
		}
	});

	result.push(statsToday);
	result.push(stats24Hours);
	result.push(stats7Days);
	result.push(stats30Days);
	result.push(statsAll);
	return result;

}

function populateStatisticsTable(listOfStatistics) {
	if (listOfStatistics == null) {
		showEmptyTable();
	} else {
		for (let statistics of listOfStatistics) {
			let tableRefActive = document.getElementById('statisticsTable').getElementsByTagName('tbody')[0];
			let row = tableRefActive.insertRow(tableRefActive.rows.length);
			row.insertCell(-1).innerHTML = statistics.timePeriod;
			row.insertCell(-1).innerHTML = statistics.numberOfActivities;
			row.insertCell(-1).innerHTML = statistics.totalTime;
			row.insertCell(-1).innerHTML = statistics.uninterruptedHours;
			row.insertCell(-1).innerHTML = (statistics.eFactor * 100).toFixed(0) + "%";
		}
	}
}

function populateWeeklyStatisticsTable(listOfWeeklyStatistics) {
	if (listOfWeeklyStatistics != null) {
		Object.keys(listOfWeeklyStatistics).forEach( key => {
			Object.keys(listOfWeeklyStatistics[key]).forEach( key2 => {
				let tableRefActive = document.getElementById('weeklyStatisticsTable').getElementsByTagName('tbody')[0];
				let row = tableRefActive.insertRow(tableRefActive.rows.length);
				row.insertCell(-1).innerHTML = key2;
				
				// Statistics column
				let hoursAndMinutes = getHoursAndMinutes(listOfWeeklyStatistics[key][key2]["totalTime"]);
				let uninterruptedHours = listOfWeeklyStatistics[key][key2]["uninterruptedHours"];
				let eFactor = getEFactor(uninterruptedHours, listOfWeeklyStatistics[key][key2]["totalTime"]);
				let statisticsHTML = hoursAndMinutes + " / " + uninterruptedHours + " / " + (eFactor * 100).toFixed(0) + "%";
				row.insertCell(-1).innerHTML = statisticsHTML;			
			});					
		});
	}
}

function showEmptyTable() {
	let hiddenRows = document.querySelectorAll(".hidden");

	for (let i = 0; i < hiddenRows.length; i++) {
		hiddenRows[i].classList.remove("hidden");
	}
}

function getStatisticsForWeeklyStats(listOfActivities) {
	let result = {};
	let lastWeek = 0;
	
	listOfActivities.forEach( function(activity)  {
		if (!activity.isActive()) {
			let week = getWeek(new Date(activity.startTime));
			if (!(week in result)) {
				/*** Create initial values in 3D object (sort of an array) ***/
				result[week] = {};
				result[week][week] = {};
				result[week][week]["totalTime"] = 0;
				result[week][week]["uninterruptedHours"] = 0;
			}
					
			result[week][week]["totalTime"] += getTotalTimeDeltaInMiliseconds(activity.startTime, activity.stopTime);
			result[week][week]["uninterruptedHours"]  += getUninterruptedHours(activity.startTime, activity.stopTime, activity.interruptions);
			
			if (activity.labels.length != 0)
			{
				activity.labels.forEach( function(string) {
					if (string in result[week])
					{
						result[week][string]["totalTime"] += getTotalTimeDeltaInMiliseconds(activity.startTime, activity.stopTime);
						result[week][string]["uninterruptedHours"] += getUninterruptedHours(activity.startTime, activity.stopTime, activity.interruptions);
					} else {
						result[week][string] = {};
						result[week][string]["totalTime"] = getTotalTimeDeltaInMiliseconds(activity.startTime, activity.stopTime);
						result[week][string]["uninterruptedHours"] = getUninterruptedHours(activity.startTime, activity.stopTime, activity.interruptions);
					}
				});
			}
		}
	});
	
	return result;
}

function localStorageCallback() {
	console.log("Local storage has been updated.")
	window.location.reload(false);
}

browser.storage.onChanged.addListener(localStorageCallback);
initialize();
