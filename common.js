"use strict";

function getTotalTimeDeltaInMiliseconds(startTime, stopTime) {
	return Math.abs(startTime - stopTime);
}

function getHoursAndMinutes(delta) {
	delta = delta / 1000; // Delta in seconds
	let hours = Math.floor(delta / 3600);
	delta -= hours * 3600;
	let minutes = Math.floor(delta / 60) % 60;

	if (hours > 99) {
		return ("0" + hours).slice(-3) + ":" + ("0" + minutes).slice(-2);
	} else {
		return ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2);
	}
}

/***
 * Source: https://stackoverflow.com/a/39502645
 */
function getWeek(date) {
	  if (!(date instanceof Date)) date = new Date();

	  // ISO week date weeks start on Monday, so correct the day number
	  var nDay = (date.getDay() + 6) % 7;

	  // ISO 8601 states that week 1 is the week with the first Thursday of that year
	  // Set the target date to the Thursday in the target week
	  date.setDate(date.getDate() - nDay + 3);

	  // Store the millisecond value of the target date
	  var n1stThursday = date.valueOf();

	  // Set the target to the first Thursday of the year
	  // First, set the target to January 1st
	  date.setMonth(0, 1);

	  // Not a Thursday? Correct the date to the next Thursday
	  if (date.getDay() !== 4) {
	    date.setMonth(0, 1 + ((4 - date.getDay()) + 7) % 7);
	  }

	  // The week number is the number of weeks between the first Thursday of the year
	  // and the Thursday in the target week (604800000 = 7 * 24 * 3600 * 1000)
	  return 1 + Math.ceil((n1stThursday - date) / 604800000);
}

function getUninterruptedHours(startTime, stopTime, interruptions) {
	let interruptionsCopy = interruptions.slice();

	interruptionsCopy.sort( function(a, b) {
		return (a.getTime() - b.getTime());
	});

	let hour = 3600 * 1000;
	let interruptCost = 20*60*1000; // Default is 20 minutes cost for interruptions

	if (startTime == null || stopTime == null) {
		return 0;
	}

	let startPlusOneHour = new Date(startTime.getTime() + hour);
	let startMinusInterruptCost = new Date(startTime - interruptCost);

	if (startPlusOneHour > stopTime) {
		return 0;
	} else if (interruptions.length == 0) {
		return getUninterruptedHours(startPlusOneHour, stopTime, interruptions) + 1;
	} else {
		let firstInterrupt = interruptionsCopy.shift(); // TODO: Assumes interruptions are sorted in time, earliest first.

		if (firstInterrupt > startMinusInterruptCost && firstInterrupt < startPlusOneHour) {
			return getUninterruptedHours(new Date(firstInterrupt.getTime() + interruptCost), stopTime, interruptionsCopy);
		} else {
			return getUninterruptedHours(startPlusOneHour, stopTime, interruptions) + 1;
		}
	}
}

function getEFactor(uninterruptedHours, totalHoursInMs) {
	return (uninterruptedHours * 3600 * 1000) / totalHoursInMs;
}

// Open extension page (either create new tab or activate existing tab)
//
function openExtensionPage(url) {
	let fullURL = browser.extension.getURL(url);

	browser.tabs.query({ url: fullURL }).then(onGot, onError);

	function onGot(tabs) {
		if (tabs.length > 0) {
			browser.tabs.update(tabs[0].id, { active: true });
		} else {
			browser.tabs.create({ url: fullURL });
		}
		window.close();
	}

	function onError(error) {
		browser.tabs.create({ url: fullURL });
		window.close();
	}
}

function parseActivities(activities) {
	console.log("Activities", activities);
	for (let activity of activities) {
		activity.startTime = new Date(activity.startTime);

		if (activity.stopTime != null) {
			activity.stopTime = new Date(activity.stopTime);
		}

		let tmpInterruptions = [];
		for (let interruption of activity.interruptions) {
			tmpInterruptions.push(new Date(interruption));
		}
		activity.interruptions = tmpInterruptions;
	}
}
