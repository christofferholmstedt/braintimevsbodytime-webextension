"use strict";

function onError(error) {
  console.log(error);
}

function initialize() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities", "labels"]);
	getActivities.then(onGotData, onError);
}

function onGotData(storageResult) {
	console.log("Got activities and labels...", storageResult);

	let tmpArray = [];
	if (storageResult.activities === undefined)
	{
		console.log("Got nothing from data store");
	} else {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}
		parseActivities(tmpArray);

	}

	// Get activity ID from GET parameter
	// and then the selected activity from data storage.
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let selectedId = searchParams.get('id');
	let selectedActivity = null;

	for (let activity of tmpArray) {
		if (selectedId == activity.id) {
			selectedActivity = activity;
		}
	}

	// Start time and stop time edit fields
	var startTimeField = document.getElementById('startTime');
	var stopTimeField = document.getElementById('stopTime');
	var startTimeButton = document.getElementById('updateStartTime');
	startTimeButton.addEventListener('click', updateStartTime);
	var stopTimeButton = document.getElementById('updateStopTime');
	stopTimeButton.addEventListener('click', updateStopTime);

	if (selectedActivity != null) {
		startTimeField.value = selectedActivity.startTime.toISOString();

		if (selectedActivity.stopTime == null) {
			stopTimeField.disabled = true;
			var stopTimeButton = document.getElementById('updateStopTime');
			stopTimeButton.disabled = true;
		} else {
			stopTimeField.value = selectedActivity.stopTime.toISOString();

			let stopTimePara = document.createElement("P");
			stopTimePara.setAttribute("class", "input-description");
			let stopTimeDesc = document.createTextNode(`Current stored start time is: ${selectedActivity.stopTime.toISOString()} `);
			stopTimePara.appendChild(stopTimeDesc);
			stopTimeButton.parentNode.insertBefore(stopTimePara, stopTimeButton.nextSibling);
		}

		let startTimePara = document.createElement("P");
		startTimePara.setAttribute("class", "input-description");
		let startTimeDesc = document.createTextNode(`Current stored start time is: ${selectedActivity.startTime.toISOString()} `);
		startTimePara.appendChild(startTimeDesc);
		startTimeButton.parentNode.insertBefore(startTimePara, startTimeButton.nextSibling);

	} else {
		startTimeField.disabled = true;
		stopTimeField.disabled = true;
		startTimeButton.disabled = true;
		stopTimeButton.disabled = true;
	}

	// Autocomplete list
	let datalist = document.createElement("datalist")
	datalist.setAttribute("id", "labelsList");

	for (let label of storageResult.labels) {
		if (!selectedActivity.labels.includes(label)) {
			let option = document.createElement("option");
			option.setAttribute("value", label);
			datalist.appendChild(option);
		}
	}

	document.body.appendChild(datalist);

	// List of labels already set for activity
	let addLabelButton = document.getElementById('addLabel');

	let labelsDiv = document.createElement("div")
	labelsDiv.setAttribute("id", "listOfLabels");

	selectedActivity.labels.sort(Intl.Collator().compare);
	for (let label of selectedActivity.labels) {
		let labelButton = document.createElement("button");
		labelButton.setAttribute("class", "button-label");
		labelButton.setAttribute("data-arg-label", label);
		labelButton.setAttribute("data-arg-id", selectedActivity.id);
		labelButton.innerHTML = `${label} <img src="icons/cancel-16-vertical-shifted.png" />`;
		labelsDiv.appendChild(labelButton);
	}

	addLabelButton.parentNode.insertBefore(labelsDiv, addLabelButton.nextSibling);

	let deleteLabelButtons = document.querySelectorAll(".button-label");

	for (let i = 0; i < deleteLabelButtons.length; i++) {
		deleteLabelButtons[i].addEventListener('click', deleteLabel);
	}

	addLabelButton.addEventListener('click', addLabel);
	addEnterKeyupEventToLabelInput();

	// List of interrupts already set for activity
	let addInteruptButton = document.getElementById('addInterrupt');

	let interruptsDiv = document.createElement("div")
	interruptsDiv.setAttribute("id", "listOfInterrupts");

	selectedActivity.interruptions.sort(Intl.Collator().compare);
	for (let interrupt of selectedActivity.interruptions) {
		let interruptButton = document.createElement("button");
		interruptButton.setAttribute("class", "button-interrupt");
		interruptButton.setAttribute("data-arg-interrupt", interrupt.toISOString());
		interruptButton.setAttribute("data-arg-id", selectedActivity.id);
		interruptButton.innerHTML = `${interrupt.toISOString()} <img src="icons/cancel-16-vertical-shifted.png" />`;
		interruptsDiv.appendChild(interruptButton);
	}

	addInteruptButton.parentNode.insertBefore(interruptsDiv, addInteruptButton.nextSibling);
	addInteruptButton.addEventListener('click', addInterrupt);

	let deleteInterruptsButtons = document.querySelectorAll(".button-interrupt");

	for (let i = 0; i < deleteInterruptsButtons.length; i++) {
		deleteInterruptsButtons[i].addEventListener('click', deleteInterrupt);
	}

}

function deleteInterrupt(event) {
	let activityId = event.target.getAttribute('data-arg-id');
	let interrupt = event.target.getAttribute('data-arg-interrupt');
	let date = new Date(interrupt);

	if (!isNaN(date.getTime())) {
		if (confirm("Are you sure you want to delete interruption \"" + interrupt + "\"?")) {

			let getActivities = browser.storage.local.get("activities");
			getActivities.then((storageResult) => {
				let tmpArray = [];
				if (storageResult.activities != undefined) {
					for (var i = 0; i < storageResult.activities.length; i++) {
						tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
					}

					for (let activity of tmpArray) {
						// Remove label from one activity
						// Assumption: Each activity can't have multiple labels
						// with same name, all of them will be removed, not just
						// one.
						if (activity.id == activityId) {
							console.log("interrupt", interrupt);
							console.log("date", date);
							activity.interruptions = activity.interruptions.filter(item => item.getTime() !== date.getTime());
						}
					}

					console.log("tmpArray", tmpArray);
					browser.storage.local.set({ "activities" : tmpArray });
				}
			}, onError);
		}
	} else {
		console.log("The interruption you are trying to delete cannot be converted to proper date");
	}
}

function addLabel() {
	console.log("Add label to activity...");
	let getActivities = browser.storage.local.get(["activities", "labels"]);
	getActivities.then(updateLabels, onError);
}

function updateLabels(storageResult) {
	console.log("Update labels...", storageResult);

	// Get activity ID from GET parameter
	// and then the selected activity from data storage.
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let selectedId = searchParams.get('id');

	let tmpArray = [];
	let label = document.getElementById('labelText').value
	if (label != "" && storageResult.activities != undefined) {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		for (let activity of tmpArray) {
			if (selectedId == activity.id && !activity.labels.includes(label)) {
				activity.labels.push(label);
			}
		}
		browser.storage.local.set({ "activities" : tmpArray });
	}

	let tmpLabelsArray = [];
	if (label != "" && storageResult.labels === undefined) {
		tmpLabelsArray.push(label);
		browser.storage.local.set({ "labels" : tmpLabelsArray });
	} else if (label != "" && storageResult.labels != undefined &&
		!storageResult.labels.includes(label)) {
		tmpLabelsArray = storageResult.labels;
		tmpLabelsArray.push(label);
		browser.storage.local.set({ "labels" : tmpLabelsArray });
	}

	document.getElementById('labelText').value = ""; // Reset value in field, no idea why this is needed. browser.storage.onChanged should be enough.
}

function deleteLabel(event) {
	let activityId = event.target.getAttribute('data-arg-id');
	let label = event.target.getAttribute('data-arg-label');

	if (confirm("Are you sure you want to delete label \"" + label + "\"?")) {

		let getActivities = browser.storage.local.get("activities");
		getActivities.then((storageResult) => {
			let tmpArray = [];
			if (storageResult.activities != undefined) {
				for (var i = 0; i < storageResult.activities.length; i++) {
					tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
				}

				for (let activity of tmpArray) {
					// Remove label from one activity
					// Assumption: Each activity can't have multiple labels
					// with same name, all of them will be removed, not just
					// one.
					if (activity.id == activityId) {
						activity.labels = activity.labels.filter(item => item != label);
					}
				}

				browser.storage.local.set({ "activities" : tmpArray });
			}
		}, onError);
	}
}

function addInterrupt() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities"]);
	getActivities.then(onGotDataAddInterrupt, onError);
}

function onGotDataAddInterrupt(storageResult) {
	console.log("Got activities...", storageResult);

	let tmpArray = [];
	if (storageResult.activities === undefined)
	{
		console.log("Got nothing from data store");
	} else {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}
	}

	// Get activity ID.
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let selectedId = searchParams.get('id');
	let interruptTimeField = document.getElementById('interruptTime');
	let date = new Date(interruptTimeField.value);

	if (!isNaN(date.getTime())) {
		for (let activity of tmpArray) {
			if (selectedId == activity.id) {
				activity.interruptions.push(date);
			}
		}
		browser.storage.local.set({ "activities" : tmpArray });
	} else {
		console.log("Timestamp is not valid, please use correct format as specified.");
	}
}

function updateStartTime() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities"]);
	getActivities.then(onGotDataUpdateStartTime, onError);
}

function onGotDataUpdateStartTime(storageResult) {
	console.log("Got activities...", storageResult);

	let tmpArray = [];
	if (storageResult.activities === undefined)
	{
		console.log("Got nothing from data store");
	} else {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}
	}

	// Get activity ID.
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let selectedId = searchParams.get('id');
	let startTimeField = document.getElementById('startTime');
	let date = new Date(startTimeField.value);

	if (!isNaN(date.getTime())) {
		for (let activity of tmpArray) {
			if (selectedId == activity.id) {
				activity.startTime = date;
			}
		}
		browser.storage.local.set({ "activities" : tmpArray });
	}
}

function updateStopTime() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities"]);
	getActivities.then(onGotDataUpdateStopTime, onError);
}

function onGotDataUpdateStopTime(storageResult) {
	console.log("Got activities...", storageResult);

	let tmpArray = [];
	if (storageResult.activities === undefined)
	{
		console.log("Got nothing from data store");
	} else {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}
	}

	// Get activity ID.
	let url = new URL(window.location.href);
	let searchParams = new URLSearchParams(url.search);
	let selectedId = searchParams.get('id');
	let stopTimeField = document.getElementById('stopTime');
	let date = new Date(stopTimeField.value);

	if (!isNaN(date.getTime())) {
		for (let activity of tmpArray) {
			if (selectedId == activity.id) {
				activity.stopTime = date;
			}
		}
		browser.storage.local.set({ "activities" : tmpArray });
	}
}

function addEnterKeyupEventToLabelInput() {
	let inputField = document.getElementById('labelText');
	inputField.focus();

	inputField.addEventListener("keyup", function (event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			addLabel();
		}
	}, false);

}

function localStorageCallback() {
	console.log("Local storage has been updated.")
	window.location.reload(false);
}

browser.storage.onChanged.addListener(localStorageCallback);
initialize();
