"use strict";

function onError(error) {
  console.log(error);
}

function initialize() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get(["activities", "labels"]);
	getActivities.then(onGotData, onError);
}

function onGotData(storageResult) {
	console.log("Got activities and labels...", storageResult);

	let tmpArray = [];
	if (storageResult.activities === undefined)
	{
		populateTables(null);
	} else {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		parseActivities(tmpArray);

		populateTables(tmpArray);
	}

	let labelsOnActiveActivity = [];

	for (let activity of tmpArray) {
		if (activity.isActive()) {
			labelsOnActiveActivity = activity.labels;
		}
	}

	if (storageResult.labels === undefined) {
		console.log("No labels");
	} else {
		console.log("Labels: ", storageResult.labels);

		let datalist = document.createElement("datalist")
		datalist.setAttribute("id", "labelsList");

		for (let label of storageResult.labels) {
			if (!labelsOnActiveActivity.includes(label)) {
				let option = document.createElement("option");
				option.setAttribute("value", label);
				datalist.appendChild(option);
			}
		}

		document.body.appendChild(datalist);
	}
}

function startActivity() {
	console.log("Start activity running...");
	let getActivities = browser.storage.local.get("activities");
	getActivities.then(storeNewActivity, onError);
}

function storeNewActivity(storageResult) {
	console.log("Store new activity...", storageResult);
	let activity = new Activity();
	activity.startTime = new Date();
	activity.id = activity.startTime.getTime();
	let tmpArray = [];

	if (storageResult.activities === undefined)
	{
		tmpArray.push(activity);
	} else {
		tmpArray = storageResult.activities;
		tmpArray.push(activity);
	}

	browser.storage.local.set({ "activities" : tmpArray });
}

function localStorageCallback() {
	console.log("Local storage has been updated.")
	window.location.reload(false);
}

function stopActivity() {
	console.log("Stop activity...");
	let getActivities = browser.storage.local.get("activities");
	getActivities.then(stopActiveActivity, onError);
}

function stopActiveActivity(storageResult) {
	console.log("Update active activity...", storageResult);

	let tmpArray = [];
	if (storageResult.activities != undefined) {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		parseActivities(tmpArray);

		for (let activity of tmpArray) {
			if (activity.isActive()) {
				activity.stopTime = new Date();
			}
		}
	}

	populateTables(tmpArray);
	browser.storage.local.set({ "activities" : tmpArray });
}

function interruptActivity() {
	console.log("Interrupt activity...");
	let getActivities = browser.storage.local.get("activities");
	getActivities.then(interruptActiveActivity, onError);
}

function interruptActiveActivity(storageResult) {
	console.log("Interrupt active activity...", storageResult);

	let tmpArray = [];
	if (storageResult.activities != undefined) {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		for (let activity of tmpArray) {
			if (activity.isActive()) {
				activity.interruptions.push(new Date());
			}
		}
	}

	browser.storage.local.set({ "activities" : tmpArray });
}

function addLabel() {
	console.log("Add label to activity...");
	let getActivities = browser.storage.local.get(["activities", "labels"]);
	getActivities.then(updateLabels, onError);
}

function updateLabels(storageResult) {
	console.log("Update labels...", storageResult);

	let tmpArray = [];
	let label = document.getElementById('labelText').value
	if (label != "" && storageResult.activities != undefined) {
		for (var i = 0; i < storageResult.activities.length; i++) {
			tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
		}

		for (let activity of tmpArray) {
			if (activity.isActive() && !activity.labels.includes(label)) {
				activity.labels.push(label);
			}
		}
		browser.storage.local.set({ "activities" : tmpArray });
	}

	let tmpLabelsArray = [];
	if (label != "" && storageResult.labels === undefined) {
		tmpLabelsArray.push(label);
		browser.storage.local.set({ "labels" : tmpLabelsArray });
	} else if (label != "" && storageResult.labels != undefined &&
		!storageResult.labels.includes(label)) {
		tmpLabelsArray = storageResult.labels;
		tmpLabelsArray.push(label);
		browser.storage.local.set({ "labels" : tmpLabelsArray });
	}
}

function deleteLabel(event) {
	let activityId = event.target.getAttribute('data-arg-id');
	let label = event.target.getAttribute('data-arg-label');

	if (confirm("Are you sure you want to delete label \"" + label + "\"?")) {

		let getActivities = browser.storage.local.get("activities");
		getActivities.then((storageResult) => {
			let tmpArray = [];
			if (storageResult.activities != undefined) {
				for (var i = 0; i < storageResult.activities.length; i++) {
					tmpArray.push(Object.assign(new Activity(), storageResult.activities[i]));
				}

				for (let activity of tmpArray) {
					// Remove label from one activity
					// Assumption: Each activity can't have multiple labels
					// with same name, all of them will be removed, not just
					// one.
					if (activity.id == activityId) {
						activity.labels = activity.labels.filter(item => item != label);
					}
				}

				browser.storage.local.set({ "activities" : tmpArray });
			}
		}, onError);
	}
}
/******************* END browser.storage usage ***************/



/* Loop through activities and modify DOM accordingly */
function populateTables(listOfActivities) {

	if (listOfActivities == null) {
		showStartActivityButton();
	} else {
		listOfActivities.sort( function(a, b) {
			return -(a.startTime.getTime() - b.startTime.getTime());
		});

		let tableRefPreviousActivies = document.getElementById('previousActiviesTable').getElementsByTagName('tbody')[0];

		let noActivityActive = true;
		let i = 0; // TODO: Should be ID stored in Activity so it is persistent between reloads
		let previousDate = new Date();
		let today = new Date();
		let todayCellPrinted = false;

		let dailyActivityCount = 0;
		let dailyTotalTime = 0;
		let dailyTotalUninterruptedHours = 0;
		
		for (let activity of listOfActivities) {
			if (activity.isActive()) {
				noActivityActive = false;
				let tableRefActive = document.getElementById('activeActivityTable').getElementsByTagName('tbody')[0];
				let row = tableRefActive.insertRow(tableRefActive.rows.length);
				row.insertCell(-1).innerHTML = activity.startTime.toString().split(" ")[4];
				let stopButtonHTML = `<button id="stopActivity" type="button" class="button">Stop activity</button>`
				row.insertCell(-1).innerHTML = stopButtonHTML;
				let labelFieldHTML = `<input type="text" id="labelText" list="labelsList" /><button id="addLabel" type="button" class="button">Add</button><br />` + splitLabelsArrayWithLinebreaks(activity.id, activity.labels)
				row.insertCell(-1).innerHTML = labelFieldHTML; // Labels
				let interruptButtonHTML = `<button id="interruptActivity" type="button" class="button">Interrupt</button><br />` + arrayOfDatesToString(activity.interruptions);
				row.insertCell(-1).innerHTML = interruptButtonHTML; // Edit
				let editButtonHTML = `<button id="editActivity${i}" type="button" class="button-edit" data-arg-id="${activity.id}">Edit activity</button>`
				row.insertCell(-1).innerHTML = editButtonHTML; // Edit

				// Add eventlisteners
				var stopActivityBtn = document.getElementById('stopActivity');
				stopActivityBtn.addEventListener('click', stopActivity);
				var interruptActivityBtn = document.getElementById('interruptActivity');
				interruptActivityBtn.addEventListener('click', interruptActivity);
				var addLabelBtn = document.getElementById('addLabel');
				addLabelBtn.addEventListener('click', addLabel);

				addEnterKeyupEventToLabelInput();
			} else {
				if (activity.startTime.getYear() == today.getYear() &&
					activity.startTime.getMonth() == today.getMonth() &&
					activity.startTime.getDate() == today.getDate() &&
					todayCellPrinted == false)
				{
					let dateRow = tableRefPreviousActivies.insertRow(tableRefPreviousActivies.rows.length);
					let dateCell = dateRow.insertCell();
					dateCell.innerHTML = "Today";
					dateCell.colSpan = 6;
					dateCell.setAttribute("class", "date-cell");

					todayCellPrinted = true;

				} else if (activity.startTime.getMonth() != previousDate.getMonth() ||
					activity.startTime.getDate() != previousDate.getDate()) {
					
					if (dailyActivityCount > 0) {
						/*** Print daily statistics ***/
						let tableRow = tableRefPreviousActivies.insertRow(tableRefPreviousActivies.rows.length);
						let statisticsRow = tableRow.insertCell();
						let dailyHoursAndMinutes = getHoursAndMinutes(dailyTotalTime);
						let dailyEFactor = getEFactor(dailyTotalUninterruptedHours, dailyTotalTime);
						statisticsRow.innerHTML = dailyHoursAndMinutes + " / " + dailyTotalUninterruptedHours + " / " + (dailyEFactor * 100).toFixed(0) + "%";
						statisticsRow.colSpan = 6;
						statisticsRow.setAttribute("class", "statistics-row");
					}
					/*** Reset daily statistics ***/
					dailyTotalTime = 0;
					dailyTotalUninterruptedHours = 0;
					dailyActivityCount = 0;
					
					/*** Print row with new data ***/
					let dateFormat = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
					let dateRow = tableRefPreviousActivies.insertRow(tableRefPreviousActivies.rows.length);
					let dateCell = dateRow.insertCell();
					dateCell.innerHTML = `${activity.startTime.toLocaleDateString('en-GB', dateFormat)}`;
					dateCell.colSpan = 6;
					dateCell.setAttribute("class", "date-cell");
					previousDate = activity.startTime;
				}

				let row = tableRefPreviousActivies.insertRow(tableRefPreviousActivies.rows.length);
				row.insertCell(-1).innerHTML = activity.startTime.toString().split(" ")[4];
				row.insertCell(-1).innerHTML = activity.stopTime.toString().split(" ")[4];
				row.insertCell(-1).innerHTML = splitLabelsArrayWithLinebreaks(activity.id, activity.labels);
				let interruptionsHTML = arrayOfDatesToString(activity.interruptions);
				row.insertCell(-1).innerHTML = interruptionsHTML;

				// Statistics column
				let deltaInMs = getTotalTimeDeltaInMiliseconds(activity.startTime, activity.stopTime);
				dailyTotalTime = dailyTotalTime + deltaInMs;
				let hoursAndMinutes = getHoursAndMinutes(deltaInMs);
				let uninterruptedHours = getUninterruptedHours(activity.startTime, activity.stopTime, activity.interruptions)
				dailyTotalUninterruptedHours = dailyTotalUninterruptedHours + uninterruptedHours;
				let eFactor = getEFactor(uninterruptedHours, deltaInMs);
				let statisticsHTML = hoursAndMinutes + " / " + uninterruptedHours + " / " + (eFactor * 100).toFixed(0) + "%";
				row.insertCell(-1).innerHTML = statisticsHTML;

				let editButtonHTML = `<button id="editActivity${i}" type="button" class="button-edit" data-arg-id="${activity.id}">Edit activity</button>`
				row.insertCell(-1).innerHTML = editButtonHTML; // Edit
				dailyActivityCount++;
				i++;
			}


		}

		let deleteLabelButtons = document.querySelectorAll(".button-label");

		for (let i = 0; i < deleteLabelButtons.length; i++) {
			deleteLabelButtons[i].addEventListener('click', deleteLabel);
		}

		let editButtons = document.querySelectorAll(".button-edit");

		for (let i = 0; i < editButtons.length; i++) {
			editButtons[i].addEventListener('click', openEditPage);
		}

		if (noActivityActive) {
			showStartActivityButton();
		}
	}
}

function splitLabelsArrayWithLinebreaks(id, labels) {
	let result = "";
	for (let label of labels) {
		result += `<button id="deleteLabel" type="button" class="button-label" data-arg-id="${id}" data-arg-label="${label}">${label} <img src="icons/cancel-16-vertical-shifted.png" /></button>` + "</br>";
	}
	return result;
}

function arrayOfDatesToString(dates) {
	let result = "";
	for (let date of dates) {
		result += date.toString().split(" ")[4].substring(0,5) + "</br>";
	}
	return result;
}

function showStartActivityButton() {
	document.getElementById('noActiveActivityRow').classList.remove("hidden");
}

function addEnterKeyupEventToLabelInput() {
	let inputField = document.getElementById('labelText');
	inputField.focus();

	inputField.addEventListener("keyup", function (event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			addLabel();
		}
	}, false);

}

// Open Edit page
function openEditPage(event) {
	let activityId = event.target.getAttribute('data-arg-id');
	openExtensionPage(`edit.html?id=${activityId}`);
}

var startActivityBtn = document.getElementById('startActivity');
startActivityBtn.addEventListener('click', startActivity);
browser.storage.onChanged.addListener(localStorageCallback);

initialize();
