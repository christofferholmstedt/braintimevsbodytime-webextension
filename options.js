"use strict";

function onError(error) {
  console.log(error);
}

function deleteAllActivities() {
	console.log("Delete all activities running...");
	let result = confirm("This will delete all logged activities, are you sure?");
	if (result) {
		browser.storage.local.set({ "activities" : [] });
	}
}

var deleteAllBtn = document.getElementById('deleteAllActivities');
deleteAllBtn.addEventListener('click', deleteAllActivities);

/// Initialization
function initialize() {
	console.log("Initializing...");
	let getActivities = browser.storage.local.get("labels");
	getActivities.then(onGotData, onError);
}

function onGotData(storageResult) {
	console.log("Got labels...", storageResult);

	if (storageResult.labels === undefined) {
		console.log("No labels");
	} else {
		console.log("Labels: ", storageResult.labels);
		let manageLabelsDesc = document.getElementById('manageLabelsDescription');

		let labelsDiv = document.createElement("div")
		labelsDiv.setAttribute("id", "listOfLabels");

		storageResult.labels.sort(Intl.Collator().compare);
		for (let label of storageResult.labels) {
			let labelButton = document.createElement("button");
			labelButton.setAttribute("class", "button-label");
			labelButton.setAttribute("data-arg-label", label);
			labelButton.innerHTML = `${label} <img src="icons/cancel-16-vertical-shifted.png" />`;
			labelsDiv.appendChild(labelButton);
		}

		manageLabelsDesc.parentNode.insertBefore(labelsDiv, manageLabelsDesc.nextSibling);

		let deleteLabelButtons = document.querySelectorAll(".button-label");

		for (let i = 0; i < deleteLabelButtons.length; i++) {
			deleteLabelButtons[i].addEventListener('click', deleteLabel);
		}
	}
}

function deleteLabel(event) {
	let label = event.target.getAttribute('data-arg-label');

	if (confirm("Are you sure you want to delete label \"" + label + "\" from autocomplete list?")) {

		let getLabels = browser.storage.local.get("labels");
		getLabels.then((storageResult) => {
			console.log("Test");
			let tmpArray = [];
			if (storageResult.labels != undefined) {
				tmpArray = storageResult.labels.filter(item => item != label);
				browser.storage.local.set({ "labels" : tmpArray });
			}
		}, onError);
	}
}

function localStorageCallback() {
	console.log("Local storage has been updated.")
	window.location.reload(false);
}

// Open Edit page
function openExportPage(event) {
	let format = event.target.getAttribute('data-arg-format');
	openExtensionPage(`exportdata.html?format=${format}`);
}

browser.storage.onChanged.addListener(localStorageCallback);
document.getElementById('exportToJSON').addEventListener('click', openExportPage);

initialize();
